import java.lang.IllegalArgumentException;

public abstract class Person {
    protected final int id;     // Id
    protected String password;  // Contraseña
    protected String name;      // Nombre
    protected String address;   // Direccion
    protected int phoneNo;      // Numero de telefono

    public static int currentIdNumber = 0;     // Unico para cada persona, incrementa con cada instancia creada que extiende esta clase abstracta

    public Person(int id, String password, String name, String address, int phoneNo) throws IllegalArgumentException {
        this.id = id;

        if (password == null || password.isEmpty()) {
            throw new IllegalArgumentException("Contraseña está vacía");
        }

        this.password = password;

        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Nombre está vacío");
        }

        this.name = name;
        this.address = address;

        if (phoneNo < 0) {
            throw new IllegalArgumentException("Número de teléfono está vacío");
        }

        this.phoneNo = phoneNo;

        setIDCount(currentIdNumber + 1);
    }

    public void printInfo() {
        System.out.println("-----------------------------------------");
        System.out.println("ID: " + id);
        System.out.println("Nombre: " + name);
        System.out.println("Direccion: " + address);
        System.out.println("Numero de telefono: " + phoneNo + "\n");
    }

    public void setAddress(String a) {
        address = a;
    }

    public void setPhone(int p) {
        phoneNo = p;
    }

    public void setName(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public int getPhoneNumber() {
        return phoneNo;
    }

    public int getID() {
        return id;
    }

    public static int getCurrentIdNumber() {
        return currentIdNumber;
    }

    public static void setIDCount(int n) {
        currentIdNumber = n;
    }

}
