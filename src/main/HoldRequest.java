import java.util.Date;

public class HoldRequest {

    private final Borrower borrower;
    private final Book book;
    private final Date requestDate;

    public HoldRequest(Borrower borrower, Book _book, Date _requestDate) {
        this.borrower = borrower;
        book = _book;
        requestDate = _requestDate;
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public Book getBook() {
        return book;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void print() {
        System.out.print(book.getTitle() + "\t\t\t\t" + borrower.getName() + "\t\t\t\t"  + requestDate + "\n");
    }
}
