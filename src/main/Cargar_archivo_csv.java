import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Cargar_archivo_csv {

    public Cargar_archivo_csv() {

    }

    public LinkedList<Persona> cargar_archivo() {
        LinkedList<Persona> lista=new LinkedList<>();
        try {
            Scanner input = new Scanner(new File("datos.txt"));
            while (input.hasNextLine()) {
                String line = input.nextLine();
                String[] datos = line.split(",");
                System.out.println(line);

                Persona nueva = new Persona(Integer.parseInt(datos[0]), datos[1], datos[2], datos[3],Integer.parseInt(datos[4].replace(" ","")));
                lista.add(nueva);


            }

            input.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return lista;
    }
}
