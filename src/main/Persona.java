public class Persona {

    public int id;     // Id
    public String password;  // Contraseña
    public String name;      // Nombre
    public String address;   // Direccion
    public int phoneNo;      // Numero de telefono

    static int currentIdNumber = 0;     // Unico para cada persona, incrementa con cada instancia creada que extiende esta clase abstracta

    public Persona(int id, String password, String name, String address, int phoneNo) {

        this.id=id;
        this.password=password;
        this.name=name;
        this.address=address;
        this.phoneNo=phoneNo;

    }

    @Override
    public String toString() {
        return "Persona{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phoneNo=" + phoneNo +
                '}';
    }
}
