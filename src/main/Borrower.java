import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Borrower extends Person {

    private ArrayList<Loan> borrowedBooks;
    private ArrayList<HoldRequest> onHoldBooks;


    public Borrower(int id, String password, String name, String address, int phoneNo) {
        super(id, password, name, address, phoneNo);

        borrowedBooks= new ArrayList<>();
        onHoldBooks = new ArrayList<>();
    }

    @Override
    public void printInfo() {
        super.printInfo();

        printBorrowedBooks();
        printOnHoldBooks();

    }

    //imprimir la info de los libros por borrower
    public void printBorrowedBooks() {
        if (!borrowedBooks.isEmpty()) {
            System.out.println("\nLos libros prestados son los siguientes: ");

            System.out.println("------------------------------------------------------------------------------");
            System.out.println("No.\t\tTitul\t\t\tAutor\t\t\tTema");
            System.out.println("------------------------------------------------------------------------------");

            for (int i = 0; i < borrowedBooks.size(); i++)
            {
                System.out.print(i + "-" + "\t\t");
                borrowedBooks.get(i).getBook().printInfo();
                System.out.print("\n");
            }
        } else {
            System.out.println("\nSin ningun libro prestado");
        }
    }


    //imprimir las info de los libros que están en espera
    public void printOnHoldBooks()
    {
        if (!onHoldBooks.isEmpty())
        {
            System.out.println("\nLibros en espera son: ");

            System.out.println("------------------------------------------------------------------------------");
            System.out.println("No.\t\tTitulo\t\t\tAutor\t\t\tTema");
            System.out.println("------------------------------------------------------------------------------");

            for (int i = 0; i < onHoldBooks.size(); i++)
            {
                System.out.print(i + "-" + "\t\t");
                onHoldBooks.get(i).getBook().printInfo();
                System.out.print("\n");
            }
        } else {
            System.out.println("\nSin libros en espera.");
        }
    }

    public void updateBorrowerInfo() throws IOException {
        String choice;

        Scanner sc = new Scanner(System.in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        System.out.println("\n¿Quieres actualizar el nombre de '" + getName() + "'? (y/n)");
        choice = sc.next();

        if(choice.equals("y")) {
            System.out.println("\nEscribe nuevo nombre: ");
            setName(reader.readLine());
            System.out.println("\nNombre correctamente actualizado.");
        }


        System.out.println("\n¿Quieres actualizar la dirección de " + getName() + "? (y/n)");
        choice = sc.next();

        if(choice.equals("y")) {
            System.out.println("\nEscribe nueva direccion: ");
            setAddress(reader.readLine());
            System.out.println("\nDireccion correctamente actualizada.");
        }

        System.out.println("\n¿Quires actualizar el número de teléfono de " + getName() + "? (y/n)");
        choice = sc.next();

        if(choice.equals("y")) {
            System.out.println("\nEscribe nuevo número de teléfono: ");
            setPhone(sc.nextInt());
            System.out.println("\nNúmero de teléfono correctamente actualizado.");
        }

        System.out.println("\nSolicitante correctamente actualizado");

    }

    public void removeBorrowedBook(Loan iBook) {
        borrowedBooks.remove(iBook);
    }

    public void addBorrowedBook(Loan iBook) {
        borrowedBooks.add(iBook);
    }

    public void addHoldRequest(HoldRequest hr) {
        onHoldBooks.add(hr);
    }

    public void removeHoldRequest(HoldRequest hr) {
        onHoldBooks.remove(hr);
    }

    public ArrayList<Loan> getBorrowedBooks() {
        return borrowedBooks;
    }

    public ArrayList<HoldRequest> getOnHoldBooks() {
        return onHoldBooks;
    }
}
