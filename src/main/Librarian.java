public class Librarian extends Staff {

    public int officeNo;     //Office Number of the Librarian
    public static int currentOfficeNumber = 0;

    public Librarian(int id,String n, String a, int p, double s,int of) {
        super(id, n, a,  a, p, s);


        if (of == -1) {
            officeNo = currentOfficeNumber;
        } else {
            officeNo = of;
        }

        currentOfficeNumber++;
    }


    public Librarian(int id,String n, String a, int p, double s,int of,Staff servicio){
        super(id, n, a,  a, p, s);
    }


    // Printing Librarian's Info

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.println("Numero de oficina: " + officeNo);
    }
}
