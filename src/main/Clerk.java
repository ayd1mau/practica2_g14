import java.lang.IllegalArgumentException;

public class Clerk extends Staff {

    int deskNo;
    public static int currentdeskNumber = 0;

    public Clerk(int id, String password, String name, String address, int phoneNo, double salary, int deskNo)  throws IllegalArgumentException{
        super(id, password, name, address, phoneNo, salary);
        this.deskNo = deskNo;
        if(deskNo == -1) {
            this.deskNo = currentdeskNumber;
        } else {
            this.deskNo=deskNo;
        }

        currentdeskNumber++;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.println("Número de escritorio: " + deskNo);
    }

    public double getDeskNo()
    {
        return this.deskNo;
    }
}
