import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.IllegalArgumentException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Book {
    private int id;
    private String title;
    private String subject;
    private String author;
    private boolean isIssued;
    private ArrayList<HoldRequest> holdRequests;

    public static int currentIdNumber = 0;

    public Book(int _id, String _title, String _subject, String _author, boolean _isissued) {
        if (_title.isEmpty()) {
            throw new IllegalArgumentException("Titulo está vacío");
        }

        if (_subject.isEmpty()) {
            throw new IllegalArgumentException("Subject está vacío");
        }

        if (_author.isEmpty()) {
            throw new IllegalArgumentException("Autor está vacío");
        }

        currentIdNumber++;

        if (_id == -1) {
            id = currentIdNumber;
        } else {
            id = _id;
        }

        title = _title;
        subject = _subject;
        author = _author;
        isIssued = _isissued;
        holdRequests = new ArrayList<>();
    }

    // Agregar un hold req.
    public void addHoldRequest(HoldRequest hr) {
        holdRequests.add(hr);
    }

    // quitar un hold req.
    public void removeHoldRequest() {
        if (!holdRequests.isEmpty()) {
            holdRequests.remove(0);
        }
    }

    // imprimir hold reqs de un libro.
    public void printHoldRequests() {
        if (!holdRequests.isEmpty()) {
            System.out.println("\nSolicitudes de espera son: ");

            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------");
            System.out.println("No.\t\tTitulo de libro\t\t\tNombre del solicitante\t\t\tFecha de solicitud");
            System.out.println("---------------------------------------------------------------------------------------------------------------------------------------");

            for (int i = 0; i < holdRequests.size(); i++) {
                System.out.print(i + "-" + "\t\t");
                holdRequests.get(i).print();
            }
        } else {
            System.out.println("\nNo hay solicitudes de espera");
        }
    }

    // acvtualizar Info de un libro
    public void changeBookInfo() throws IOException {
        Scanner scanner = new Scanner(System.in);
        String input;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("\n¿Actualizar Autor? (y/n)");
        input = scanner.next();

        if (input.equals("y")) {
            System.out.println("\nIngrese nuevo Autor: ");
            author = reader.readLine();
        }

        System.out.println("\n¿Actualizar Tema? (y/n)");
        input = scanner.next();

        if (input.equals("y")) {
            System.out.println("\nIngrese nuevo Tema: ");
            subject = reader.readLine();
        }

        System.out.println("\n¿Actualizar Titulo? (y/n)");
        input = scanner.next();

        if (input.equals("y")) {
            System.out.println("\nIngrese nuevo titulo: ");
            title = reader.readLine();
        }

        System.out.println("\nLibro se actualizo correctamente.");

    }

    // Imprimir info de libro
    public void printInfo() {
        System.out.println(title + "\t\t\t" + author + "\t\t\t" + subject);
    }

    public int getID() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSubject() {
        return subject;
    }

    public String getAuthor() {
        return author;
    }

    public boolean getIsIssued() {
        return isIssued;
    }

    public ArrayList<HoldRequest> getHoldRequests() {
        return holdRequests;
    }

    public void setHoldRequests(HoldRequest nuevo){ this.holdRequests.add(nuevo); }

    public void setIssuedStatus(boolean s) {
        isIssued = s;
    }

    // Setter
    public static void setIDCount(int n) {
        currentIdNumber = n;
    }

    // Poner el libro en espera
    public void placeBookOnHold(Borrower bor)
    {
        HoldRequest hr = new HoldRequest(bor,this, new Date());

        addHoldRequest(hr);        //Agregar esta solicitud de reserva para mantener la cola de solitudes
        bor.addHoldRequest(hr);    //Agregar esta solicitud de reserva a la clase de ese prestario particular

        System.out.println("\nEl libro " + title + " se ha puesto en espera por " + bor.getName() + ".\n");
    }

    // Solicitud para tener un libro
    public void makeHoldRequest(Borrower borrower) {
        boolean makeRequest = true;

        // Si el prestario ya ha solicitado el mismo libro.
        // Entonces no puede solicitar ese libro. Tendra que renovar
        // el libro para ampliar el plazo de devolucion
        for (int i = 0; i < borrower.getBorrowedBooks().size(); i++) {
            if (borrower.getBorrowedBooks().get(i).getBook() == this) {
                System.out.println("\n" + "Ya has pedido el prestamo " + title);
                return;
            }
        }

        //Si el prestario ya ha solicitado el mismo libro.
        //Entonces no se le permite volver a hacer la misma solicitud
        for (int i = 0; i < holdRequests.size(); i++) {
            if ((holdRequests.get(i).getBorrower() == borrower)) {
                makeRequest = false;
                break;
            }
        }

        if (makeRequest) {
            placeBookOnHold(borrower);
        } else {
            System.out.println("\nYa tienes una solicitud de reserva para este libro.\n");
        }
    }

    // Otener informacion de una solicitud de reserva
    public void serviceHoldRequest(HoldRequest hr) {
        removeHoldRequest();
        hr.getBorrower().removeHoldRequest(hr);
    }

    // Emitir un libro
    public void issueBook(Borrower borrower, Staff staff) {
        // Primero se elimina las solicitudes de reserva
        Date today = new Date();

        ArrayList<HoldRequest> hRequests = holdRequests;

        for (int i = 0; i < hRequests.size(); i++) {
            HoldRequest hr = hRequests.get(i);

            // Eliminar las reservas que ha expirado
            long days =  ChronoUnit.DAYS.between(today.toInstant(), hr.getRequestDate().toInstant());
            days = -days;

            if(days>Library.getInstance().getHoldRequestExpiry()) {
                removeHoldRequest();
                hr.getBorrower().removeHoldRequest(hr);
            }
        }

        if (isIssued) {
            System.out.println("\nEl libro " + title + " ya se encuentra publicado.");
            System.out.println("¿Deseas poner el libro en espera? (y/n)");

            Scanner sc = new Scanner(System.in);
            String choice = sc.next();

            if (choice.equals("y")) {
                makeHoldRequest(borrower);
            }
        } else {
            if (!holdRequests.isEmpty()) {
                boolean hasRequest = false;

                for (int i = 0; i < holdRequests.size() && !hasRequest;i++) {
                    if (holdRequests.get(i).getBorrower() == borrower) {
                        hasRequest = true;
                    }
                }

                if (hasRequest) {
                    // Si este prestatario en particular  tiene la solicitud de este libro
                    if (holdRequests.get(0).getBorrower() == borrower) {
                        serviceHoldRequest(holdRequests.get(0));
                    } else {
                        System.out.println("\nLo sentimos, otros usuarios ya han solicitado este libro, debes esperar a que se procesen sus solicitudes.");
                        return;
                    }
                } else {
                    System.out.println("\nOtros usuarias ya han solicitado este libro por lo cual no podemos entregartelo");

                    System.out.println("¿Deseas agregar tu solicitud de espera? (y/n)");

                    Scanner sc = new Scanner(System.in);
                    String choice = sc.next();

                    if (choice.equals("y")) {
                        makeHoldRequest(borrower);
                    }

                    return;
                }
            }

            // Si no hay reservas para este libro, simplemente se emita el libros
            setIssuedStatus(true);

            Loan iHistory = new Loan(borrower,this,staff,null,new Date(),null,false);

            Library.getInstance().addLoan(iHistory);
            borrower.addBorrowedBook(iHistory);

            System.out.println("\nEl libro " + title + " fue exitosamente solicitado por " + borrower.getName() + ".");
            System.out.println("\nPublicado por: " + staff.getName());
        }
    }


    // Returning a Book
    public void returnBook(Borrower borrower, Loan l, Staff staff) {
        l.getBook().setIssuedStatus(false);
        l.setReturnedDate(new Date());
        l.setReceiver(staff);

        borrower.removeBorrowedBook(l);

        l.payFine();

        System.out.println("\nEl libro " + l.getBook().getTitle() + " fue exitosamente devuelto por " + borrower.getName() + ".");
        System.out.println("\nRecibido por: " + staff.getName());
    }
}




