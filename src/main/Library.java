import java.util.ArrayList;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.sql.Types;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Library {

    private String name;
    private Librarian librarian;
    private ArrayList<Person> persons;
    private ArrayList<Book> booksInLibrary;

    private ArrayList<Loan> loans;

    private static Library obj;
    public int book_return_deadline;
    public double per_day_fine;

    public int hold_request_expiry;

    public static Library getInstance() {
        if (obj == null) {
            obj = new Library();
        }

        return obj;
    }

    private Library()   // constantes por defecto
    {
        name = null;
        librarian = null;
        persons = new ArrayList();

        booksInLibrary = new ArrayList();
        loans = new ArrayList();
    }


    /*------------funciones set------------*/

    public void setReturnDeadline(int deadline) {
        book_return_deadline = deadline;
    }

    public void setFine(double perDayFine) {
        per_day_fine = perDayFine;
    }

    public void setRequestExpiry(int hrExpiry) {
        hold_request_expiry = hrExpiry;
    }
    /*--------------------------------------*/


    public void setName(String n) {
        name = n;
    }

    /*-----------Funciones get------------*/

    public int getHoldRequestExpiry() {
        return hold_request_expiry;
    }

    public ArrayList<Person> getPersons() {
        return persons;
    }

    public Librarian getLibrarian() {
        return librarian;
    }

    public String getLibraryName() {
        return name;
    }

    public ArrayList<Book> getBooks() {
        return booksInLibrary;
    }

    /*---------------------------------------*/

    /*-----Adding all People in Library----*/
    public boolean addLibrarian(Librarian lib) {
        //One Library can have only one Librarian
        if (librarian == null) {
            librarian = lib;
            persons.add(librarian);
            return true;
        } else
            System.out.println("\nLa biblioteca ya tiene un bibliotecario. No se puede crear un bibliotecario nuevo.");
        return false;
    }

    public void addClerk(Clerk c) {
        persons.add(c);
    }

    public void addBorrower(Borrower b) {
        persons.add(b);
    }


    public void addLoan(Loan l) {
        loans.add(l);
    }

    /*----------------------------------------------*/

    /*-----------Encontrar personas en la biblioteca--------------*/
    public Borrower findBorrower() {
        System.out.println("\nIngrese Borrower ID: ");

        int id = 0;

        Scanner scanner = new Scanner(System.in);

        try {
            id = scanner.nextInt();
        } catch (java.util.InputMismatchException e) {
            System.out.println("\nEntrada inválida");
        }

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getID() == id && persons.get(i).getClass().getSimpleName().equals("Borrower"))
                return (Borrower) (persons.get(i));
        }

        System.out.println("\nLo sentimos, este ID no coincide con ningun Borrower ID.");
        return null;
    }

    public Clerk findClerk() {
        System.out.println("\nIngrese Clerk ID: ");

        int id = 0;

        Scanner scanner = new Scanner(System.in);

        try {
            id = scanner.nextInt();
        } catch (java.util.InputMismatchException e) {
            System.out.println("\nEntrada inválida");
        }

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getID() == id && persons.get(i).getClass().getSimpleName().equals("Clerk"))
                return (Clerk) (persons.get(i));
        }

        System.out.println("\nLo sentimos, este ID no coincide con ningun Clerk ID.");
        return null;
    }

    /*-------  Funciones en libros en la biblioteca--------------*/
    public void addBookinLibrary(Book b) {
        booksInLibrary.add(b);
    }

    public void removeBookfromLibrary(Book b) {
        boolean delete = true;

        for (int i = 0; i < persons.size() && delete; i++) {
            if (persons.get(i).getClass().getSimpleName().equals("Borrower")) {
                ArrayList<Loan> borBooks = ((Borrower) (persons.get(i))).getBorrowedBooks();

                for (int j = 0; j < borBooks.size() && delete; j++) {
                    if (borBooks.get(j).getBook() == b) {
                        delete = false;
                        System.out.println("Este libro en particular está actualmente prestado por algún borrower.");
                    }
                }
            }
        }

        if (delete) {
            System.out.println("\nActualmente, este libro no es prestado por nadie.");
            ArrayList<HoldRequest> hRequests = b.getHoldRequests();

            if (!hRequests.isEmpty()) {
                System.out.println("\nAlgunos borrowers pueden solicitar este libro en espera. Eliminar este libro también eliminará las solicitudes de reserva relevantes.");
                System.out.println("¿Aún quieres eliminar el libro? (s/n)");

                Scanner sc = new Scanner(System.in);

                while (true) {
                    String choice = sc.next();

                    if (choice.equals("s") || choice.equals("n")) {
                        if (choice.equals("n")) {
                            System.out.println("\nEliminación sin éxito.");
                            return;
                        } else {

                            for (int i = 0; i < hRequests.size() && delete; i++) {
                                HoldRequest hr = hRequests.get(i);
                                hr.getBorrower().removeHoldRequest(hr);
                                b.removeHoldRequest();
                            }
                        }
                    } else
                        System.out.println("Entrada inválida. Enter (s/n): ");
                }

            } else
                System.out.println("Este libro no tiene solicitudes de reserva.");

            booksInLibrary.remove(b);
            System.out.println("El libro se ha eliminado correctamente.");
        } else
            System.out.println("\nEliminación sin éxito.");
    }


    public ArrayList<Book> searchForBooks() throws IOException {
        String choice;
        String title = "", subject = "", author = "";

        Scanner sc = new Scanner(System.in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.println("\nIngrese '1' o '2' o '3' para buscar por título, tema o autor del libro respectivamente: ");
            choice = sc.next();

            if (choice.equals("1") || choice.equals("2") || choice.equals("3"))
                break;
            else
                System.out.println("\nEntrada incorrecta!");
        }

        if (choice.equals("1")) {
            System.out.println("\nIngrese el título del libro: ");
            title = reader.readLine();
        } else if (choice.equals("2")) {
            System.out.println("\nIngrese el tema del libro: ");
            subject = reader.readLine();
        } else {
            System.out.println("\nIngrese el autor del libro: ");
            author = reader.readLine();
        }

        ArrayList<Book> matchedBooks = new ArrayList();


        for (int i = 0; i < booksInLibrary.size(); i++) {
            Book b = booksInLibrary.get(i);

            if (choice.equals("1")) {
                if (b.getTitle().equals(title))
                    matchedBooks.add(b);
            } else if (choice.equals("2")) {
                if (b.getSubject().equals(subject))
                    matchedBooks.add(b);
            } else {
                if (b.getAuthor().equals(author))
                    matchedBooks.add(b);
            }
        }


        if (!matchedBooks.isEmpty()) {
            System.out.println("\nEstos libros se encuentran: \n");

            System.out.println("------------------------------------------------------------------------------");
            System.out.println("No.\t\tTitulo\t\t\tAutor\t\t\tTema");
            System.out.println("------------------------------------------------------------------------------");

            for (int i = 0; i < matchedBooks.size(); i++) {
                System.out.print(i + "-" + "\t\t");
                matchedBooks.get(i).printInfo();
                System.out.print("\n");
            }

            return matchedBooks;
        } else {
            System.out.println("\nLo sentimos. No se encontraron libros relacionados con su consulta.");
            return null;
        }
    }


    public void viewAllBooks() {
        if (!booksInLibrary.isEmpty()) {
            System.out.println("\nLos libros son:: ");

            System.out.println("------------------------------------------------------------------------------");
            System.out.println("No.\t\tTitulo\t\t\tAutor\t\t\tTema");
            System.out.println("------------------------------------------------------------------------------");

            for (int i = 0; i < booksInLibrary.size(); i++) {
                System.out.print(i + "-" + "\t\t");
                booksInLibrary.get(i).printInfo();
                System.out.print("\n");
            }
        } else
            System.out.println("\nActualmente, la biblioteca no tiene libros.");
    }


    public double computeFine2(Borrower borrower) {
        System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("No.\t\tEl titulo del libro\t\tNombre Borrower\t\t\tFecha de emisión\t\t\tFecha de devolución\t\t\t\tMulta(Rs)");
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        double totalFine = 0;
        double per_loan_fine = 0;

        for (int i = 0; i < loans.size(); i++) {
            Loan l = loans.get(i);

            if ((l.getBorrower() == borrower)) {
                per_loan_fine = l.computeFine1();
                System.out.print(i + "-" + "\t\t" + loans.get(i).getBook().getTitle() + "\t\t\t" + loans.get(i).getBorrower().getName() + "\t\t" + loans.get(i).getIssuedDate() + "\t\t\t" + loans.get(i).getReturnDate() + "\t\t\t\t" + per_loan_fine + "\n");

                totalFine += per_loan_fine;
            }
        }

        return totalFine;
    }


    public void createPerson(char x) {
        Scanner sc = new Scanner(System.in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("\nIngrese Nombre: ");
        String n = "";
        try {
            n = reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Ingrese Dirección: ");
        String address = "";
        try {
            address = reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Library.class.getName()).log(Level.SEVERE, null, ex);
        }

        int phone = 0;

        try {
            System.out.println("Ingrese número telefonico: ");
            phone = sc.nextInt();
        } catch (java.util.InputMismatchException e) {
            System.out.println("\nEntrada inválida.");
        }


        if (x == 'c') {
            double salary = 0;

            try {
                System.out.println("Ingrese salario: ");
                salary = sc.nextDouble();
            } catch (java.util.InputMismatchException e) {
                System.out.println("\nEntrada inválida.");
            }

            Clerk c = new Clerk(-1, "pass", n, address, phone, salary, -1);
            addClerk(c);

            System.out.println("\nClerk con nombre: " + n + " creado satisfactoriamente.");
            System.out.println("\nSu ID es : " + c.getID());
            System.out.println("Su Contraseña es : " + c.getPassword());
        } else if (x == 'l') {
            double salary = 0;
            try {
                System.out.println("Ingrese salario: ");
                salary = sc.nextDouble();
            } catch (java.util.InputMismatchException e) {
                System.out.println("\nEntrada inválida.");
            }

            Librarian l = new Librarian(-1, n, address, phone, salary, -1);
            if (addLibrarian(l)) {
                System.out.println("\nBibliotecario con nombre " + n + " creado con éxito.");
                System.out.println("\nSu ID es : " + l.getID());
                System.out.println("su Contraseña es : " + l.getPassword());
            }
        }

        //If borrower is to be created
        else {
            Borrower b = new Borrower(-1, "", n, address, phone);
            addBorrower(b);
            System.out.println("\nBorrower con nombre " + n + " creado exitosamente.");

            System.out.println("\nSu ID es : " + b.getID());
            System.out.println("Su contraseña es : " + b.getPassword());
        }
    }


    public void createBook(String title, String subject, String author) {
        Book b = new Book(-1, title, subject, author, false);

        addBookinLibrary(b);

        System.out.println("\nLibro con titulo " + b.getTitle() + " se creó con éxito.");
    }


    // Called when want an access to Portal
    public Person login() {
        Scanner input = new Scanner(System.in);

        int id = 0;
        String password = "";

        System.out.println("\nIngrese ID: ");

        try {
            id = input.nextInt();
        } catch (java.util.InputMismatchException e) {
            System.out.println("\nentrada inválida");
        }

        System.out.println("Ingrese Contraseña: ");
        password = input.next();

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getID() == id && persons.get(i).getPassword().equals(password)) {
                System.out.println("\nLogin exitosamente");
                return persons.get(i);
            }
        }

        if (librarian != null) {
            if (librarian.getID() == id && librarian.getPassword().equals(password)) {
                System.out.println("\nLogin exitosamente");
                return librarian;
            }
        }

        System.out.println("\n¡Lo sentimos! ID o Contraseña equivocada");
        return null;
    }


    // History when a Book was Issued and was Returned!
    public void viewHistory() {
        if (!loans.isEmpty()) {
            System.out.println("\nLos libros publicados son: ");

            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");
            System.out.println("No.\tTítulo del libro\tBorrower nombre\t  Nombre del emisor\t\tFecha de emisión\t\t\tNombre del destinatario\t\tFecha de devolución\t\tmulta pagada");
            System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------");

            for (int i = 0; i < loans.size(); i++) {
                if (loans.get(i).getIssuer() != null)
                    System.out.print(i + "-" + "\t" + loans.get(i).getBook().getTitle() + "\t\t\t" + loans.get(i).getBorrower().getName() + "\t\t" + loans.get(i).getIssuer().getName() + "\t    " + loans.get(i).getIssuedDate());

                if (loans.get(i).getReceiver() != null) {
                    System.out.print("\t" + loans.get(i).getReceiver().getName() + "\t\t" + loans.get(i).getReturnDate() + "\t   " + loans.get(i).getFineStatus() + "\n");
                } else
                    System.out.print("\t\t" + "--" + "\t\t\t" + "--" + "\t\t" + "--" + "\n");
            }
        } else
            System.out.println("\nSin libros publicados.");
    }


    //---------------------------------------------------------------------------------------//
    /*--------------------------------COLABORACIÓN CON BASE DE DATOS------------------------------------------*/

    // Making Connection With Database
    public Connection makeConnection() {
        try {
            String host = "jdbc:mysql://167.99.237.132:3306/LMS";
            String uName = "ayd1_2";
            String uPass = "JQbKrrwv2^V#xTEV2";
            return DriverManager.getConnection(host, uName, uPass);
        } catch (SQLException err) {
            System.out.println(err.getMessage());
            return null;
        }
    }


    // Loading all info in code via Database.
    public void populateLibrary(Connection con) throws SQLException, IOException {
        Library lib = this;
        Statement stmt = con.createStatement();

        /* --- Populating Book ----*/
        String SQL = "SELECT * FROM BOOK";
        ResultSet rs = stmt.executeQuery(SQL);

        if (!rs.next()) {
            System.out.println("\nNo se encontraron libros en la biblioteca");
        } else {
            int maxID = 0;

            do {
                if (rs.getString("TITLE") != null && rs.getString("AUTHOR") != null && rs.getString("SUBJECT") != null && rs.getInt("ID") != 0) {
                    String title = rs.getString("TITLE");
                    String author = rs.getString("AUTHOR");
                    String subject = rs.getString("SUBJECT");
                    int id = rs.getInt("ID");
                    boolean issue = rs.getBoolean("IS_ISSUED");
                    Book b = new Book(id, title, subject, author, issue);
                    addBookinLibrary(b);

                    if (maxID < id)
                        maxID = id;
                }
            } while (rs.next());

            // setting Book Count
            Book.setIDCount(maxID);
        }

        /* ----Populating Clerks----*/

        SQL = "SELECT ID,PNAME,ADDRESS,PASSWORD,PHONE_NO,SALARY,DESK_NO FROM PERSON INNER JOIN CLERK ON ID=C_ID INNER JOIN STAFF ON S_ID=C_ID";

        rs = stmt.executeQuery(SQL);

        if (!rs.next()) {
            System.out.println("No se encontraron empleados en la biblioteca");
        } else {
            do {
                int id = rs.getInt("ID");
                String cname = rs.getString("PNAME");
                String adrs = rs.getString("ADDRESS");
                int phn = rs.getInt("PHONE_NO");
                double sal = rs.getDouble("SALARY");
                int desk = rs.getInt("DESK_NO");
                Clerk c = new Clerk(id, "pass", cname, adrs, phn, sal, desk);

                addClerk(c);
            }
            while (rs.next());

        }

        /*-----Populating Librarian---*/
        SQL = "SELECT ID,PNAME,ADDRESS,PASSWORD,PHONE_NO,SALARY,OFFICE_NO FROM PERSON INNER JOIN LIBRARIAN ON ID=L_ID INNER JOIN STAFF ON S_ID=L_ID";

        rs = stmt.executeQuery(SQL);
        if (!rs.next()) {
            System.out.println("No se ha encontrado ningún bibliotecario en la biblioteca");
        } else {
            do {
                int id = rs.getInt("ID");
                String lname = rs.getString("PNAME");
                String adrs = rs.getString("ADDRESS");
                int phn = rs.getInt("PHONE_NO");
                double sal = rs.getDouble("SALARY");
                int off = rs.getInt("OFFICE_NO");
                Librarian l = new Librarian(id, lname, adrs, phn, sal, off);

                addLibrarian(l);

            } while (rs.next());

        }

        /*---Populating Borrowers (partially)!!!!!!--------*/

        SQL = "SELECT ID,PNAME,ADDRESS,PASSWORD,PHONE_NO FROM PERSON INNER JOIN BORROWER ON ID=B_ID";

        rs = stmt.executeQuery(SQL);

        if (!rs.next()) {
            System.out.println("No se encontró ningún Borrower en la biblioteca");
        } else {
            do {
                int id = rs.getInt("ID");
                String name = rs.getString("PNAME");
                String adrs = rs.getString("ADDRESS");
                int phn = rs.getInt("PHONE_NO");

                Borrower b = new Borrower(id, "pass", name, adrs, phn);
                addBorrower(b);

            } while (rs.next());

        }

        /*----Populating Loan----*/

        SQL = "SELECT * FROM LOAN";

        rs = stmt.executeQuery(SQL);
        if (!rs.next()) {
            System.out.println("¡Aún no se han publicado libros!");
        } else {
            do {
                int borid = rs.getInt("BORROWER");
                int bokid = rs.getInt("BOOK");
                int iid = rs.getInt("ISSUER");
                Integer rid = (Integer) rs.getObject("RECEIVER");
                int rd = 0;
                Date rdate;

                Date idate = new Date(rs.getTimestamp("ISS_DATE").getTime());

                if (rid != null)    // if there is a receiver
                {
                    rdate = new Date(rs.getTimestamp("RET_DATE").getTime());
                    rd = (int) rid;
                } else {
                    rdate = null;
                }

                boolean fineStatus = rs.getBoolean("FINE_PAID");

                boolean set = true;

                Borrower bb = null;


                for (int i = 0; i < getPersons().size() && set; i++) {
                    if (getPersons().get(i).getID() == borid) {
                        set = false;
                        bb = (Borrower) (getPersons().get(i));
                    }
                }

                set = true;
                Staff s[] = new Staff[2];

                if (iid == getLibrarian().getID()) {
                    s[0] = getLibrarian();
                } else {
                    for (int k = 0; k < getPersons().size() && set; k++) {
                        if (getPersons().get(k).getID() == iid && getPersons().get(k).getClass().getSimpleName().equals("Clerk")) {
                            set = false;
                            s[0] = (Clerk) (getPersons().get(k));
                        }
                    }
                }

                set = true;
                // If not returned yet...
                if (rid == null) {
                    s[1] = null;  // no reciever
                    rdate = null;
                } else {
                    if (rd == getLibrarian().getID())
                        s[1] = getLibrarian();

                    else {    //System.out.println("ff");
                        for (int k = 0; k < getPersons().size() && set; k++) {
                            if (getPersons().get(k).getID() == rd && getPersons().get(k).getClass().getSimpleName().equals("Clerk")) {
                                set = false;
                                s[1] = (Clerk) (getPersons().get(k));
                            }
                        }
                    }
                }

                set = true;

                ArrayList<Book> books = getBooks();

                for (int k = 0; k < books.size() && set; k++) {
                    if (books.get(k).getID() == bokid) {
                        set = false;
                        Loan l = new Loan(bb, books.get(k), s[0], s[1], idate, rdate, fineStatus);
                        loans.add(l);
                    }
                }

            } while (rs.next());
        }
        /*----Populationg Hold Books----*/

        SQL = "SELECT * FROM ON_HOLD_BOOK";

        rs = stmt.executeQuery(SQL);
        if (!rs.next()) {
            System.out.println("¡Aún no hay libros en espera!");
        } else {
            do {
                int borid = rs.getInt("BORROWER");
                int bokid = rs.getInt("BOOK");
                Date off = new Date(rs.getDate("REQ_DATE").getTime());

                boolean set = true;
                Borrower bb = null;

                ArrayList<Person> persons = lib.getPersons();

                for (int i = 0; i < persons.size() && set; i++) {
                    if (persons.get(i).getID() == borid) {
                        set = false;
                        bb = (Borrower) (persons.get(i));
                    }
                }

                set = true;

                ArrayList<Book> books = lib.getBooks();

                for (int i = 0; i < books.size() && set; i++) {
                    if (books.get(i).getID() == bokid) {
                        set = false;
                        HoldRequest hbook = new HoldRequest(bb, books.get(i), off);
                        books.get(i).addHoldRequest(hbook);
                        bb.addHoldRequest(hbook);
                    }
                }
            } while (rs.next());
        }

        /* --- Populating Borrower's Remaining Info----*/

        // Borrowed Books
        SQL = "SELECT ID,BOOK FROM PERSON INNER JOIN BORROWER ON ID=B_ID INNER JOIN BORROWED_BOOK ON B_ID=BORROWER ";

        rs = stmt.executeQuery(SQL);

        if (!rs.next()) {
            System.out.println("Ningún borrower ha tomado prestado todavía de la biblioteca.");
        } else {

            do {
                int id = rs.getInt("ID");      // borrower
                int bid = rs.getInt("BOOK");   // book

                Borrower bb = null;
                boolean set = true;
                boolean okay = true;

                for (int i = 0; i < lib.getPersons().size() && set; i++) {
                    if (lib.getPersons().get(i).getClass().getSimpleName().equals("Borrower")) {
                        if (lib.getPersons().get(i).getID() == id) {
                            set = false;
                            bb = (Borrower) (lib.getPersons().get(i));
                        }
                    }
                }

                set = true;

                ArrayList<Loan> books = loans;

                for (int i = 0; i < books.size() && set; i++) {
                    if (books.get(i).getBook().getID() == bid && books.get(i).getReceiver() == null) {
                        set = false;
                        Loan bBook = new Loan(bb, books.get(i).getBook(), books.get(i).getIssuer(), null, books.get(i).getIssuedDate(), null, books.get(i).getFineStatus());
                        bb.addBorrowedBook(bBook);
                    }
                }

            } while (rs.next());
        }

        ArrayList<Person> persons = lib.getPersons();

        /* Setting Person ID Count */
        int max = 0;

        for (int i = 0; i < persons.size(); i++) {
            if (max < persons.get(i).getID())
                max = persons.get(i).getID();
        }

        Person.setIDCount(max);
    }


    // Filling Changes back to Database
    public void fillItBack(Connection con) throws SQLException, SQLIntegrityConstraintViolationException {
        /*-----------Loan Table Cleared------------*/

        String template = "DELETE FROM LIBRARY.LOAN";
        PreparedStatement stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------Borrowed Books Table Cleared------------*/

        template = "DELETE FROM LIBRARY.BORROWED_BOOK";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------OnHoldBooks Table Cleared------------*/

        template = "DELETE FROM LIBRARY.ON_HOLD_BOOK";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------Books Table Cleared------------*/

        template = "DELETE FROM LIBRARY.BOOK";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------Clerk Table Cleared------------*/

        template = "DELETE FROM LIBRARY.CLERK";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------Librarian Table Cleared------------*/

        template = "DELETE FROM LIBRARY.LIBRARIAN";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------Borrower Table Cleared------------*/

        template = "DELETE FROM LIBRARY.BORROWER";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------Staff Table Cleared------------*/

        template = "DELETE FROM LIBRARY.STAFF";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        /*-----------Person Table Cleared------------*/

        template = "DELETE FROM LIBRARY.PERSON";
        stmts = con.prepareStatement(template);

        stmts.executeUpdate();

        Library lib = this;

        /* Filling Person's Table*/
        for (int i = 0; i < lib.getPersons().size(); i++) {
            template = "INSERT INTO LIBRARY.PERSON (ID,PNAME,PASSWORD,ADDRESS,PHONE_NO) values (?,?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(template);

            stmt.setInt('1', lib.getPersons().get(i).getID());
            stmt.setString('2', lib.getPersons().get(i).getName());
            stmt.setString('3', lib.getPersons().get(i).getPassword());
            stmt.setString('4', lib.getPersons().get(i).getAddress());
            stmt.setInt('5', lib.getPersons().get(i).getPhoneNumber());

            stmt.executeUpdate();
        }

        /* Filling Clerk's Table and Staff Table*/
        for (int i = 0; i < lib.getPersons().size(); i++) {
            if (lib.getPersons().get(i).getClass().getSimpleName().equals("Clerk")) {
                template = "INSERT INTO LIBRARY.STAFF (S_ID,TYPE,SALARY) values (?,?,?)";
                PreparedStatement stmt = con.prepareStatement(template);

                stmt.setInt('1', lib.getPersons().get(i).getID());
                stmt.setString('2', "Clerk");
                stmt.setDouble('3', ((Clerk) (lib.getPersons().get(i))).getSalary());

                stmt.executeUpdate();

                template = "INSERT INTO LIBRARY.CLERK (C_ID,DESK_NO) values (?,?)";
                stmt = con.prepareStatement(template);

                stmt.setInt('1', lib.getPersons().get(i).getID());
                stmt.setInt('2', ((Clerk) (lib.getPersons().get(i))).deskNo);

                stmt.executeUpdate();
            }

        }

        if (lib.getLibrarian() != null)    // if  librarian is there
        {
            template = "INSERT INTO LIBRARY.STAFF (S_ID,TYPE,SALARY) values (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(template);

            stmt.setInt('1', lib.getLibrarian().getID());
            stmt.setString('2', "Librarian");
            stmt.setDouble('3', lib.getLibrarian().getSalary());

            stmt.executeUpdate();

            template = "INSERT INTO LIBRARY.LIBRARIAN (L_ID,OFFICE_NO) values (?,?)";
            stmt = con.prepareStatement(template);

            stmt.setInt('1', lib.getLibrarian().getID());
            stmt.setInt('2', lib.getLibrarian().officeNo);

            stmt.executeUpdate();
        }

        /* Filling Borrower's Table*/
        for (int i = 0; i < lib.getPersons().size(); i++) {
            if (lib.getPersons().get(i).getClass().getSimpleName().equals("Borrower")) {
                template = "INSERT INTO LIBRARY.BORROWER(B_ID) values (?)";
                PreparedStatement stmt = con.prepareStatement(template);

                stmt.setInt('1', lib.getPersons().get(i).getID());

                stmt.executeUpdate();
            }
        }

        ArrayList<Book> books = lib.getBooks();

        /*Filling Book's Table*/
        for (int i = 0; i < books.size(); i++) {
            template = "INSERT INTO LIBRARY.BOOK (ID,TITLE,AUTHOR,SUBJECT,IS_ISSUED) values (?,?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(template);

            stmt.setInt('1', books.get(i).getID());
            stmt.setString('2', books.get(i).getTitle());
            stmt.setString('3', books.get(i).getAuthor());
            stmt.setString('4', books.get(i).getSubject());
            stmt.setBoolean('5', books.get(i).getIsIssued());
            stmt.executeUpdate();

        }

        /* Filling Loan Book's Table*/
        for (int i = 0; i < loans.size(); i++) {
            template = "INSERT INTO LIBRARY.LOAN(L_ID,BORROWER,BOOK,ISSUER,ISS_DATE,RECEIVER,RET_DATE,FINE_PAID) values (?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = con.prepareStatement(template);

            stmt.setInt('1', i + 1);
            stmt.setInt('2', loans.get(i).getBorrower().getID());
            stmt.setInt('3', loans.get(i).getBook().getID());
            stmt.setInt('4', loans.get(i).getIssuer().getID());
            stmt.setTimestamp('5', new java.sql.Timestamp(loans.get(i).getIssuedDate().getTime()));
            stmt.setBoolean('8', loans.get(i).getFineStatus());
            if (loans.get(i).getReceiver() == null) {
                stmt.setNull('6', Types.INTEGER);
                stmt.setDate('7', null);
            } else {
                stmt.setInt('6', loans.get(i).getReceiver().getID());
                stmt.setTimestamp('7', new java.sql.Timestamp(loans.get(i).getReturnDate().getTime()));
            }

            stmt.executeUpdate();

        }

        /* Filling On_Hold_ Table*/

        int x = 1;
        for (int i = 0; i < lib.getBooks().size(); i++) {
            for (int j = 0; j < lib.getBooks().get(i).getHoldRequests().size(); j++) {
                template = "INSERT INTO LIBRARY.ON_HOLD_BOOK(REQ_ID,BOOK,BORROWER,REQ_DATE) values (?,?,?,?)";
                PreparedStatement stmt = con.prepareStatement(template);

                stmt.setInt('1', x);
                stmt.setInt('3', lib.getBooks().get(i).getHoldRequests().get(j).getBorrower().getID());
                stmt.setInt('2', lib.getBooks().get(i).getHoldRequests().get(j).getBook().getID());
                stmt.setDate('4', new java.sql.Date(lib.getBooks().get(i).getHoldRequests().get(j).getRequestDate().getTime()));

                stmt.executeUpdate();
                x++;

            }
        }

        /* Filling Borrowed Book Table*/
        for (int i = 0; i < lib.getBooks().size(); i++) {
            if (lib.getBooks().get(i).getIsIssued() == true) {
                boolean set = true;
                for (int j = 0; j < loans.size() && set; j++) {
                    if (lib.getBooks().get(i).getID() == loans.get(j).getBook().getID()) {
                        if (loans.get(j).getReceiver() == null) {
                            template = "INSERT INTO LIBRARY.BORROWED_BOOK(BOOK,BORROWER) values (?,?)";
                            PreparedStatement stmt = con.prepareStatement(template);
                            stmt.setInt('1', loans.get(j).getBook().getID());
                            stmt.setInt('2', loans.get(j).getBorrower().getID());

                            stmt.executeUpdate();
                            set = false;
                        }
                    }

                }

            }
        }
    } // Filling Done!

}
