import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Scanner;
public class Loan {
    private Borrower borrower;
    private Book book;

    private Staff issuer;
    private Date issuedDate;

    private Date dateReturned;
    private Staff receiver;

    private boolean finePaid;

    public Loan(Borrower borrower, Book book, Staff issuer, Staff receiver, Date iDate, Date rDate, boolean fPaid)
    {
        this.borrower = borrower;
        this.book = book;
        this.issuer = issuer;
        this.receiver = receiver;
        this.issuedDate = iDate;
        this.dateReturned = rDate;
        this.finePaid = fPaid;
    }

    /*----- Funciones get------------*/

    public Book getBook()       //Returna book
    {
        return book;
    }

    public Staff getIssuer()     //Devuelve el miembro del personal que emitió el libro.
    {
        return issuer;
    }

    public Staff getReceiver()  //Devuelve el miembro del personal al que se le devuelve el libro
    {
        return receiver;
    }

    public Date getIssuedDate()     //Devuelve la fecha en la que se publicó este libro en particular.
    {
        return issuedDate;
    }

    public Date getReturnDate()     //Devuelve la fecha en la que se devolvió este libro en particular.
    {
        return dateReturned;
    }

    public Borrower getBorrower()   //Devuelve el prestatario a quien se emitió el libro
    {
        return borrower;
    }

    public boolean getFineStatus()  // Devuelve el estado de la multa
    {
        return finePaid;
    }


    /*----------Funciones set---------------------*/
    public void setReturnedDate(Date dReturned)
    {
        dateReturned = dReturned;
    }

    public void setFineStatus(boolean fStatus)
    {
        finePaid = fStatus;
    }

    public void setReceiver(Staff r)
    {
        receiver = r;
    }
    public void setBook(Book book)
    {
        this.book = book;
    }

    //calcula multa solo para un préstamo en particular
    public double computeFine1()
    {

        double totalFine = 0;

        if (!finePaid)
        {
            Date iDate = issuedDate;
            Date rDate = new Date();

            long days =  ChronoUnit.DAYS.between(rDate.toInstant(), iDate.toInstant());
            days=0-days;

            days = days - Library.getInstance().book_return_deadline;

            if(days>0)
                totalFine = days * Library.getInstance().per_day_fine;
            else
                totalFine=0;
        }
        return totalFine;
    }


    public void payFine()
    {

        double totalFine = computeFine1();

        if (totalFine > 0)
        {
            System.out.println("\nMulta total generada: Rs " + totalFine);

            System.out.println("¿Quieres pagar? (s/n)");

            Scanner input = new Scanner(System.in);

            String choice = input.next();

            if(choice.equals("s") || choice.equals("S"))
                finePaid = true;

            if(choice.equals("n") || choice.equals("N"))
                finePaid = false;
        }
        else
        {
            System.out.println("\nNo se genera ninguna multa.");
            finePaid = true;
        }
    }


    // Ampliación de la fecha de emisión
    public void renewIssuedBook(Date iDate)
    {
        issuedDate = iDate;

        System.out.println("\nLa fecha límite del libro " + getBook().getTitle() + " ha sido extendido.");
        System.out.println("¡El libro emitido se ha renovado con éxito!\n");
    }

}
