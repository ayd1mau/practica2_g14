import java.io.*;
import java.util.*;
import java.sql.*;

public class main
{
    // Clearing Required Area of Screen
    public static void clrscr()
    {
        for (int i = 0; i < 20; i++)
            System.out.println();
    }

    // Asking for Input as Choice
    public static int takeInput(int min, int max)
    {
        String choice;
        Scanner input = new Scanner(System.in);

        while(true)
        {
            System.out.println("\nIngrese opcion: ");

            choice = input.next();

            if((!choice.matches(".*[a-zA-Z]+.*")) && (Integer.parseInt(choice) > min && Integer.parseInt(choice) < max))
            {
                return Integer.parseInt(choice);
            }

            else
                System.out.println("\nEntrada invalida.");
        }

    }

    // Functionalities of all Persons
    public static void allFunctionalities(Person person, int choice) throws IOException
    {
        Library lib = Library.getInstance();

        Scanner scanner = new Scanner(System.in);
        int input = 0;

        //Search Book
        if (choice == 1)
        {
            lib.searchForBooks();
        }

        //Do Hold Request
        else if (choice == 2)
        {
            ArrayList<Book> books = lib.searchForBooks();

            if (books != null)
            {
                input = takeInput(-1,books.size());

                Book b = books.get(input);

                if("Clerk".equals(person.getClass().getSimpleName()) || "Librarian".equals(person.getClass().getSimpleName()))
                {
                    Borrower bor = lib.findBorrower();

                    if (bor != null)
                        b.makeHoldRequest(bor);
                }
                else
                    b.makeHoldRequest((Borrower)person);
            }
        }

        //View borrower's personal information
        else if (choice == 3)
        {
            if("Clerk".equals(person.getClass().getSimpleName()) || "Librarian".equals(person.getClass().getSimpleName()))
            {
                Borrower bor = lib.findBorrower();

                if(bor!=null)
                    bor.printInfo();
            }
            else
                person.printInfo();
        }

        //Compute Fine of a Borrower
        else if (choice == 4)
        {
            if("Clerk".equals(person.getClass().getSimpleName()) || "Librarian".equals(person.getClass().getSimpleName()))
            {
                Borrower bor = lib.findBorrower();

                if(bor!=null)
                {
                    double totalFine = lib.computeFine2(bor);
                    System.out.println("\nEl total de tu multa son : GTQ " + totalFine );
                }
            }
            else
            {
                double totalFine = lib.computeFine2((Borrower)person);
                System.out.println("\nEl total de tu multa es : GTQ " + totalFine );
            }
        }

        //Check hold request queue of a book
        else if (choice == 5)
        {
            ArrayList<Book> books = lib.searchForBooks();

            if (books != null)
            {
                input = takeInput(-1,books.size());
                books.get(input).printHoldRequests();
            }
        }

        //Issue a Book
        else if (choice == 6)
        {
            ArrayList<Book> books = lib.searchForBooks();

            if (books != null)
            {
                input = takeInput(-1,books.size());
                Book b = books.get(input);

                Borrower bor = lib.findBorrower();

                if(bor!=null)
                {
                    b.issueBook(bor, (Staff)person);
                }
            }
        }

        //Return a Book
        else if (choice == 7)
        {
            Borrower bor = lib.findBorrower();

            if(bor!=null)
            {
                bor.printBorrowedBooks();
                ArrayList<Loan> loans = bor.getBorrowedBooks();

                if (!loans.isEmpty())
                {
                    input = takeInput(-1,loans.size());
                    Loan l = loans.get(input);

                    l.getBook().returnBook(bor, l, (Staff)person);
                }
                else
                    System.out.println("\nEste solicitante " + bor.getName() + " no tiene ningun libro por devolver.");
            }
        }

        //Renew a Book
        else if (choice == 8)
        {
            Borrower bor = lib.findBorrower();

            if(bor!=null)
            {
                bor.printBorrowedBooks();
                ArrayList<Loan> loans = bor.getBorrowedBooks();

                if (!loans.isEmpty())
                {
                    input = takeInput(-1,loans.size());

                    loans.get(input).renewIssuedBook(new java.util.Date());
                }
                else
                    System.out.println("\nEste solicitante " + bor.getName() + " no tiene ningun libro para renovar.");
            }
        }

        //Add new Borrower
        else if (choice == 9)
        {
            lib.createPerson('b');
        }

        //Update Borrower's Personal Info
        else if (choice == 10)
        {
            Borrower bor = lib.findBorrower();

            if(bor != null)
                bor.updateBorrowerInfo();
        }

        //Add new Book
        else if (choice == 11)
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("\nIngrese titulo:");
            String title = reader.readLine();

            System.out.println("\nIngrese tema:");
            String subject = reader.readLine();

            System.out.println("\nIngrese autor:");
            String author = reader.readLine();

            lib.createBook(title, subject, author);
        }

        //Remove a Book
        else if (choice == 12)
        {
            ArrayList<Book> books = lib.searchForBooks();

            if (books != null)
            {
                input = takeInput(-1,books.size());

                lib.removeBookfromLibrary(books.get(input));
            }
        }

        //Change a Book's Info
        else if (choice == 13)
        {
            ArrayList<Book> books = lib.searchForBooks();

            if (books!=null)
            {
                input = takeInput(-1,books.size());

                books.get(input).changeBookInfo();
            }
        }

        //View clerk's personal information
        else if (choice == 14)
        {
            Clerk clerk = lib.findClerk();

            if(clerk!=null)
                clerk.printInfo();
        }

        // Functionality Performed.
        System.out.println("\nPresione cualquier tecla para continuar..\n");
        scanner.next();
    }






    /*-------------------------------------MAIN---------------------------------------------------*/

    public static void main(String[] args)
    {
        Scanner admin = new Scanner(System.in);

        //-------------------INTERFACE---------------------------//

        Library lib = Library.getInstance();

        // Setting some by default information like name of library ,fine, deadline and limit of hold request
        lib.setFine(20);
        lib.setRequestExpiry(7);
        lib.setReturnDeadline(5);
        lib.setName("Libreria AYD1");

        // Making connection with Database.
        Connection con = lib.makeConnection();

        if (con == null)    // Oops can't connnect !
        {
            System.out.println("\nError conectandose a la base de datos, saliendo...");
            return;
        }

        try {

            lib.populateLibrary(con);   // Populating Library with all Records

            boolean stop = false;
            while(!stop)
            {
                clrscr();

                // FRONT END //
                System.out.println("--------------------------------------------------------");
                System.out.println("\tBienvenidos al sistema de control de libreria");
                System.out.println("--------------------------------------------------------");

                System.out.println("Las siguientes funcionalidades estan disponibles: \n");
                System.out.println("1- Inicio de sesion");
                System.out.println("2- Salir");
                System.out.println("3- Funciones administrativas"); // Administration has access only

                System.out.println("-----------------------------------------\n");

                int choice = 0;

                choice = takeInput(0,4);

                if (choice == 3)
                {
                    System.out.println("\nIngrese contraseña de root: ");
                    String aPass = admin.next();

                    if(aPass.equals("lib"))
                    {
                        while (true)    // Way to Admin Portal
                        {
                            clrscr();

                            System.out.println("--------------------------------------------------------");
                            System.out.println("\tBienvenido al portal de administradores");
                            System.out.println("--------------------------------------------------------");
                            System.out.println("Las siguientes funcionalidades estan disponibles: \n");

                            System.out.println("1- Agregar empleados");
                            System.out.println("2- Agregar bibliotecarios");
                            System.out.println("3- Historial de libros entregados");
                            System.out.println("4- Ver todos los libros en la biblioteca");
                            System.out.println("5- Salir");

                            System.out.println("---------------------------------------------");

                            choice = takeInput(0,6);

                            if (choice == 5)
                                break;

                            if (choice == 1)
                                lib.createPerson('c');
                            else if (choice == 2)
                                lib.createPerson('l');

                            else if (choice == 3)
                                lib.viewHistory();

                            else if (choice == 4)
                                lib.viewAllBooks();

                            System.out.println("\nPresione cualquier tecla para continuar..\n");
                            admin.next();
                        }
                    }
                    else
                        System.out.println("\nLo sentimos, contraseña incorrecta.");
                }

                else if (choice == 1)
                {
                    Person person = lib.login();

                    if (person == null){}

                    else if (person.getClass().getSimpleName().equals("Borrower"))
                    {
                        while (true)    // Way to Borrower's Portal
                        {
                            clrscr();

                            System.out.println("--------------------------------------------------------");
                            System.out.println("\tBienvenido al portal de Solicitantes");
                            System.out.println("--------------------------------------------------------");
                            System.out.println("1- Buscar un libro");
                            System.out.println("2- Poner solicitud de espera");
                            System.out.println("3- Ver informacion personal");
                            System.out.println("4- Ver multas pendientes");
                            System.out.println("5- Ver listados de solicitantes en espera por libro");
                            System.out.println("6- Salir");
                            System.out.println("--------------------------------------------------------");

                            choice = takeInput(0,7);

                            if (choice == 6)
                                break;

                            allFunctionalities(person,choice);
                        }
                    }

                    else if (person.getClass().getSimpleName().equals("Clerk"))
                    {
                        while(true) // Way to Clerk's Portal
                        {
                            clrscr();

                            System.out.println("--------------------------------------------------------");
                            System.out.println("\tBienvenido al portal de Empleado");
                            System.out.println("--------------------------------------------------------");
                            System.out.println("1- Buscar libro3");
                            System.out.println("2- Colocar un libro en espera");
                            System.out.println("3- Ver información de solicitante");
                            System.out.println("4- Ver multas de solicitante");
                            System.out.println("5- Ver cola de espera de libro");
                            System.out.println("6- Registrar entrega de libro");
                            System.out.println("7- Registrar devolucion de libro");
                            System.out.println("8- Renovar libro");
                            System.out.println("9- Agregar nuevo solicitante");
                            System.out.println("10- Actualizar informacion de solicitante");
                            System.out.println("11- Salir");
                            System.out.println("--------------------------------------------------------");

                            choice = takeInput(0,12);

                            if (choice == 11)
                                break;

                            allFunctionalities(person,choice);
                        }
                    }

                    else if (person.getClass().getSimpleName().equals("Librarian"))
                    {
                        while(true) // Way to Librarian Portal
                        {
                            clrscr();

                            System.out.println("--------------------------------------------------------");
                            System.out.println("\tBienvenido al portal de Bibliotecario");
                            System.out.println("--------------------------------------------------------");
                            System.out.println("1- Buscar libro");
                            System.out.println("2- Colocar un libro en espera");
                            System.out.println("3- Ver informacion de solicitante");
                            System.out.println("4- Ver total de multas de solicitante");
                            System.out.println("5- Ver lista de espera para libro");
                            System.out.println("6- Registrar entrega de libro");
                            System.out.println("7- Registrar devolucion de libro");
                            System.out.println("8- Renovar un libro");
                            System.out.println("9- Agregar un nuevo solicitante");
                            System.out.println("10- Actualizar información de solicitante");
                            System.out.println("11- Agregar un libro");
                            System.out.println("12- Eliminar un libro");
                            System.out.println("13- Cambiar informacion de libro");
                            System.out.println("14- Ver información de empleados");
                            System.out.println("15- Salir");
                            System.out.println("--------------------------------------------------------");

                            choice = takeInput(0,16);

                            if (choice == 15)
                                break;

                            allFunctionalities(person,choice);
                        }
                    }

                }

                else
                    stop = true;

                System.out.println("\nPresione cualquier tecla para continuar..\n");
                Scanner scanner = new Scanner(System.in);
                scanner.next();
            }

            //Loading back all the records in database
            lib.fillItBack(con);
        }
        catch(Exception e)
        {
            System.out.println("\nAdios :)...\n");
        }   // System Closed!

    }    // Main Closed

}   // Class closed.
