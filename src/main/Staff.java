public class Staff extends Person {
    public double salary;

    public Staff(int id, String password, String name, String address, int phoneNo, double salary)
    {
        super(id,password,name,address,phoneNo);
        this.salary = salary;
    }

    @Override
    public void printInfo()
    {
        super.printInfo();
        System.out.println("Salario: " + salary + "\n");
    }

    public double getSalary()
    {
        return salary;
    }

    public String imprimir_info(){
        System.out.println("El salario es: "+salary);
        return "El salario es: "+salary;
    }

}
