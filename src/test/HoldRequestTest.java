import java.lang.IllegalArgumentException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HoldRequestTest {

    public HoldRequest holdRequestMock;

    @Before
    public void setup() {
        holdRequestMock = mock(HoldRequest.class);
    }

    @Test
    public void test_valid_borrower() throws IllegalArgumentException {
        // Arrange
        Borrower expectedBorrower = new Borrower(2, "asdf123", "Prueba Prueba", "Zona12", 12345678);
        when(holdRequestMock.getBorrower()).thenReturn(expectedBorrower);
        // Act
        Borrower receivedBorrower = holdRequestMock.getBorrower();
        // Assert
        Assert.assertEquals(expectedBorrower.getID(), receivedBorrower.getID());
    }

    @Test
    public void test_valid_book() throws IllegalArgumentException {
        // Arrange
        Book expectedBook = new Book(1, "Test", "Subject", "Mauricio", false);
        when(holdRequestMock.getBook()).thenReturn(expectedBook);
        // Act
        Book receivedBook = holdRequestMock.getBook();
        // Assert
        Assert.assertEquals(expectedBook.getID(), receivedBook.getID());
    }

    @Test
    public void test_valid_requestDate() throws IllegalArgumentException {
        // Arrange
        Date expectedDate = new Date();
        when(holdRequestMock.getRequestDate()).thenReturn(expectedDate);
        // Act
        Date receivedDate = holdRequestMock.getRequestDate();
        // Assert
        Assert.assertEquals(expectedDate.getTime(), receivedDate.getTime());
    }

}
