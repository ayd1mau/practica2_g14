
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.lang.IllegalArgumentException;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LibraryTest {
    public Library libraryMock;
    public Librarian librarian;
    @Before
    public void setup() {
         this.libraryMock=mock(Library.class);
    }

    //como ya hay un librarian regresa false
    @Test
    public void test_addLibrarian() throws IllegalArgumentException {
        Librarian expectedlibrarian =new Librarian(1,"Test","Test",12345678,7000,1);
        when(libraryMock.addLibrarian(expectedlibrarian)).thenReturn(true);

        boolean res=libraryMock.addLibrarian(new Librarian(2,"Test2","Test2",22245678,9000,1));
        Assert.assertNotEquals(true, res);
    }

    @Test
    public void test_getLibrarian() throws IllegalArgumentException {
        Librarian expectedlibrarian =new Librarian(1,"Test","Test",12345678,7000,1);
        when(libraryMock.getLibrarian()).thenReturn(expectedlibrarian);

        Librarian res=libraryMock.getLibrarian();
        Assert.assertEquals(expectedlibrarian.name, res.name);
    }

}
