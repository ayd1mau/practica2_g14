import com.sun.corba.se.impl.encoding.CDROutputObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.LinkedList;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

public class ClerkTest {
    protected double salary;
    protected int id;
    protected String password;
    protected String name;
    protected String address;
    protected int phoneNo;
    protected int deskNo;

    protected Clerk mockcleer;
    protected Staff mokckstaff;
    protected Cargar_archivo_csv mockCargar;
    protected LinkedList<Persona> lista;
    @Before
    public void setup(){
        salary = 2500.50;
        id = 20;
        password = "test";
        name = "test";
        address = "test";
        phoneNo = 10;
        deskNo = 7;
        mokckstaff= Mockito.mock(Staff.class);
        mockcleer= Mockito.mock(Clerk.class);
        mockCargar=Mockito.mock(Cargar_archivo_csv.class);

        lista= mockCargar.cargar_archivo();



    }

    //validar si al ingresar el número de escritorio: -1 regresa él currentdeskNumber
    @Test
    public void test_valid_DeskNo() throws IllegalArgumentException {
        for (Persona element: lista

        ) {
            System.out.println(element.id);
        }
        int noDesk=-1;
        ClerkMock clerkMock = new ClerkMock(id, password, name, address, phoneNo,salary,noDesk);
        int expectedDeskNumber=ClerkMock.currentdeskNumber-1;
        Assert.assertEquals(expectedDeskNumber,clerkMock.getDeskNo(),0);
    }


    //validar que al ingresar el número de escritorio retorne el número correcto
    @Test
    public void Test_DeskNo() throws IllegalArgumentException{
        int expectedDeskNo = 7;
        ClerkMock clerkMock = new ClerkMock(id,password,name,address,phoneNo,salary,this.deskNo);

        Assert.assertEquals(expectedDeskNo,clerkMock.getDeskNo(),0);
    }


    @Test
    public void verificar_elflujo_printingo() {
        Mockito.when(mokckstaff.getAddress()).thenReturn("");



        Assert.assertTrue(!(mokckstaff.equals("El escritorio es: "+ mockcleer.getDeskNo())));

    }




    @Test
    public void test_sum_current() {
        // Arrange
        int expectedPhoneNumber = 44675262;

        mockcleer = new Clerk(this.id,"prueba2",this.name,this.address,this.phoneNo,this.salary,this.deskNo);

        // Assert
        Assert.assertEquals(1.0,Clerk.currentIdNumber,0);
    }


    @Test
    public void   getDeskNoTest() {
        // Arrange
        int expectedPhoneNumber = 44675262;

        mockcleer = new Clerk(this.id,"prueba2",this.name,this.address,this.phoneNo,this.salary,this.deskNo);

        // Assert
        Assert.assertEquals(mockcleer.deskNo,mockcleer.getDeskNo(),0);
    }





}
