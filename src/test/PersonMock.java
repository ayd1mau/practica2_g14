public class PersonMock extends Person {
    public PersonMock(int id, String password, String name, String address, int phoneNo) throws IllegalArgumentException {
        super(id, password, name, address, phoneNo);
    }
}
