import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.mockito.Mockito.mock;

public class BorrowerTest {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private final InputStream sysInBackup = System.in; // Guardar System.in para restaurarlo luego
    private final ByteArrayInputStream in = new ByteArrayInputStream("".getBytes());

    public Borrower borrowerMock;

    @Before
    public void setUp() {
        System.setIn(in);
        System.setOut(new PrintStream(outputStreamCaptor));
        borrowerMock = mock(Borrower.class);
    }

    @After
    public void tearDown() {
        System.setIn(sysInBackup);
        System.setOut(standardOut);
    }

    @Test
    public void test_getOnHoldBooks() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = mock(Borrower.class);
        // Act
        Assert.assertNotNull(borrowerMock.getOnHoldBooks());
        // Assert
        // Will throw
    }

    @Test
    public void test_getOnHoldBooks_isArray() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = mock(Borrower.class);
        // Act
        Assert.assertEquals(new ArrayList<>(),borrowerMock.getOnHoldBooks());
        // Assert
        // Will throw
    }

    @Test
    public void test_getBorrowedBooks() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = mock(Borrower.class);
        // Act
        Assert.assertNotNull(borrowerMock.getBorrowedBooks());
        // Assert
        // Will throw
    }

    @Test
    public void test_getBorrowedBooks_isArray() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = mock(Borrower.class);
        // Act
        Assert.assertEquals(new ArrayList<>(),borrowerMock.getBorrowedBooks());
        // Assert
        // Will throw
    }

    @Test
    public void test_addHoldRequest() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = new Borrower(-1,"!234","test","Guatemala",1234);
        HoldRequest holdRequestMockMock = mock(HoldRequest.class);
        // Act
        borrowerMock.addHoldRequest(holdRequestMockMock);
        // Assert
        Assert.assertNotEquals(new ArrayList<>(),borrowerMock.getOnHoldBooks());
        // Will throw
    }

    @Test
    public void test_addBorrowedBook() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = new Borrower(-1,"!234","test","Guatemala",1234);
        Loan loanBookMock = mock(Loan.class);
        // Act
        borrowerMock.addBorrowedBook(loanBookMock);
        // Assert
        Assert.assertNotEquals(new ArrayList<>(),borrowerMock.getBorrowedBooks());
        // Will throw
    }

    @Test
    public void test_removeBorrowedBook() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = new Borrower(-1,"!234","test","Guatemala",1234);
        Loan loanBookMock = mock(Loan.class);
        borrowerMock.addBorrowedBook(loanBookMock);
        // Act
        borrowerMock.removeBorrowedBook(loanBookMock);
        // Assert
        Assert.assertEquals(new ArrayList<>(),borrowerMock.getBorrowedBooks());
        // Will throw
    }

    @Test
    public void test_removeHoldRequest() throws IllegalArgumentException {
        // Arrange
        Borrower borrowerMock = new Borrower(-1,"!234","test","Guatemala",1234);
        HoldRequest holdRequestMockMock = mock(HoldRequest.class);
        borrowerMock.addHoldRequest(holdRequestMockMock);
        // Act
        borrowerMock.removeHoldRequest(holdRequestMockMock);
        // Assert
        Assert.assertEquals(new ArrayList<>(),borrowerMock.getOnHoldBooks());
        // Will throw
    }


}
