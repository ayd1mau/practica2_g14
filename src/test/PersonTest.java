import java.lang.IllegalArgumentException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PersonTest {

    protected int id;
    protected String password;
    protected String name;
    protected String address;
    protected int phoneNo;
    protected int currentIdNumber;

    public PersonMock personMock;

    @Before
    public void setup() {
        id = 1;
        password = "Test";
        name = "Test";
        address = "Test";
        phoneNo = 44675262;
        currentIdNumber = 0;
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_valid_phone_number() throws IllegalArgumentException {
        // Arrange
        phoneNo = -1;
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        // Will throw
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_valid_name() throws IllegalArgumentException {
        // Arrange
        name = "";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        // Will throw
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_valid_password() throws IllegalArgumentException {
        // Arrange
        password = "";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        // Will throw
    }

    @Test
    public void test_valid_address() throws IllegalArgumentException {
        // Arrange
        address = "";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertTrue(personMock.getAddress().isEmpty());
    }

    @Test
    public void test_id_is_same_as_set_in_constructor() throws IllegalArgumentException {
        // Arrange
        int expectedId = 50;
        id = 50;
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedId, personMock.getID());
    }

    @Test
    public void test_password_is_same_as_set_in_constructor() throws IllegalArgumentException {
        // Arrange
        String expectedPassword = "Test password";
        password = "Test password";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedPassword, personMock.getPassword());
    }

    @Test
    public void test_name_is_same_as_set_in_constructor() throws IllegalArgumentException {
        // Arrange
        String expectedName = "Test LastnameTest";
        name = "Test LastnameTest";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedName, personMock.getName());
    }

    @Test
    public void test_address_is_same_as_set_in_constructor() throws IllegalArgumentException {
        // Arrange
        String expectedAddress = "Zona 12 Usac";
        address = "Zona 12 Usac";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedAddress, personMock.getAddress());
    }

    @Test
    public void test_phone_is_same_as_set_in_constructor() throws IllegalArgumentException {
        // Arrange
        int expectedPhone = 55566677;
        phoneNo = 55566677;
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedPhone, personMock.getPhoneNumber());
    }

    @Test
    public void test_set_address_updates_person_address() {
        // Arrange
        String expectedAddress = "Test";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedAddress, personMock.getAddress());

        expectedAddress = "New Test Address";
        personMock.setAddress(expectedAddress);

        Assert.assertEquals(expectedAddress, personMock.getAddress());
    }

    @Test
    public void test_set_phone_updates_person_phone() {
        // Arrange
        int expectedPhoneNumber = 44675262;
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedPhoneNumber, personMock.getPhoneNumber());

        expectedPhoneNumber = 9999999;
        personMock.setPhone(expectedPhoneNumber);

        Assert.assertEquals(expectedPhoneNumber, personMock.getPhoneNumber());
    }

    @Test
    public void test_set_name_updates_person_name() {
        // Arrange
        String expectedName = "Test";
        // Act
        personMock = new PersonMock(id, password, name, address, phoneNo);
        // Assert
        Assert.assertEquals(expectedName, personMock.getName());

        expectedName = "ThisIsMyNameForReal";
        personMock.setName(expectedName);

        Assert.assertEquals(expectedName, personMock.getName());
    }

    @Test
    public void test_currentId_is_same_as_set_in_constructor() {
        // Arrange
        int expectedCurrentIdNumber = 26;
        // Act
        int currentIdNumber = PersonMock.getCurrentIdNumber();
        // Assert
        Assert.assertEquals(expectedCurrentIdNumber, currentIdNumber);
    }

}
