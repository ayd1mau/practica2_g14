public class ClerkMock extends Clerk {
    public ClerkMock(int id, String password, String name, String address, int phoneNo, double salary, int deskNo)  throws IllegalArgumentException{
        super(id, password, name, address, phoneNo, salary, deskNo);
    }
}
