import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.lang.IllegalArgumentException;

public class StaffTest {
    protected double salary;
    protected int id;
    protected String password;
    protected String name;
    protected String address;
    protected int phoneNo;

    @Before
    public void setup(){
        salary = 2500.50;
        id = 20;
        password = "test";
        name = "test";
        address = "test";
        phoneNo = 10;
    }

    @Test
    public void Test_Salary() throws IllegalArgumentException{
        double expectedSalary = 2500.50;
        this.salary = 2500.50;

        Staff nuevo = new Staff(id,password,name,address,phoneNo,this.salary); //CREATE A NEW STAFF

        Assert.assertEquals(expectedSalary,nuevo.getSalary(),0.00);
    }


}
