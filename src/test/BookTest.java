import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class BookTest {

    protected int id;
    protected String title;
    protected String subject;
    protected String author;
    protected boolean isIssued;
    protected ArrayList<HoldRequest> holdRequests;

    public Book bookMock;
    public Borrower prestatario;
    public Book nuevo;

    @Before
    public void setup() {
        this.id = -1;
        this.title = "Ready Player One";
        this.subject = "Ciencia Ficción";
        this.author = "Ernest Cline";
        this.isIssued = false;
        this.prestatario= new Borrower(-1,"1234","test","Guatemala", 12345);
        this.nuevo = new Book(2,"The Lord of the rings","Ciencia Ficción","Tolkien",true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_valid_empty_title() throws IllegalArgumentException {
        // Arrange
        title = "";
        // Act
        bookMock = new Book(this.id, title, this.subject, this.author, this.isIssued);
        // Assert
        // Will throw
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_valid_empty_subject() throws IllegalArgumentException {
        // Arrange
        subject = "";
        // Act
        bookMock = new Book(this.id, this.title, subject, this.author, this.isIssued);
        // Assert
        // Will throw
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_valid_empty_author() throws IllegalArgumentException {
        // Arrange
        author = "";
        // Act
        bookMock = new Book(this.id, this.title, this.subject, author, this.isIssued);
        // Assert
        // Will throw
    }

    @Test
    public void test_valid_next_id_number() throws IllegalArgumentException {
        //Arrange
        int current_val = Book.currentIdNumber;
        // Act
        bookMock = new Book(this.id, title, this.subject, this.author, this.isIssued);
        // Assert
        Assert.assertEquals(current_val+1, bookMock.getID());
    }

    @Test
    public void test_valid_title() throws IllegalArgumentException {
        // Act
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        // Assert
        Assert.assertEquals(this.title, bookMock.getTitle());
    }

    @Test
    public void test_valid_subject() throws IllegalArgumentException {
        // Act
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        // Assert
        Assert.assertEquals(this.subject, bookMock.getSubject());
    }

    @Test
    public void test_valid_author() throws IllegalArgumentException {
        // Act
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        // Assert
        Assert.assertEquals(this.author, bookMock.getAuthor());
    }

    @Test
    public void test_valid_isissue() throws IllegalArgumentException {
        // Act
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        // Assert
        Assert.assertEquals(this.isIssued, bookMock.getIsIssued());
    }

    @Test
    public void test_isArrayList_HoldRequest() throws IllegalArgumentException {
        //arrenge
        ArrayList<HoldRequest> mockHoldRequests = new ArrayList();
        // Act
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        // Assert
        Assert.assertEquals(mockHoldRequests, bookMock.getHoldRequests());
    }

    @Test
    public void test_setIssuedStatus() throws IllegalArgumentException {
        //arrenge
        Boolean prevIssued = this.isIssued;
        // Act
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        bookMock.setIssuedStatus(true);
        // Assert
        Assert.assertEquals(true, bookMock.getIsIssued());
    }

    @Test
    public void test_addholdRequest() throws IllegalArgumentException {
        //arrenge
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        Borrower borrower = new Borrower(-1,"1234","test","Guatemala", 12345);
        HoldRequest hold = new HoldRequest(borrower,bookMock,new Date());
        ArrayList<HoldRequest> mockHoldRequests = new ArrayList();
        mockHoldRequests.add(hold);
        // Act
        bookMock.addHoldRequest(hold);
        // Assert
        Assert.assertEquals(mockHoldRequests, bookMock.getHoldRequests());
    }

    @Test
    public void test_removeHoldRequest() throws IllegalArgumentException {
        //arrenge
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);
        Borrower borrower = new Borrower(-1,"1234","test","Guatemala", 12345);
        HoldRequest hold = new HoldRequest(borrower,bookMock,new Date());
//        ArrayList<HoldRequest> emptyMockHoldRequests = new ArrayList();
        ArrayList<HoldRequest> mockHoldRequests = new ArrayList();
        mockHoldRequests.add(hold);
        bookMock.addHoldRequest(hold);
        // Act
        bookMock.removeHoldRequest();
        // Assert
        Assert.assertNotEquals(mockHoldRequests, bookMock.getHoldRequests());
    }

    @Test
        public void test_placeBookOnHold() throws  IllegalArgumentException{
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);

        bookMock.placeBookOnHold(this.prestatario);
        HoldRequest hr = new HoldRequest(this.prestatario,bookMock, new Date());

        Assert.assertNotSame("No son el mismo objeto, uno es una lista de Holdrequest",
                this.prestatario.getOnHoldBooks(),hr);
    }

    // Prueba para verificar si la condicion para renovar un libro
    @Test
    public void test_makeHoldRequest() throws  IllegalArgumentException{
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);

        Borrower borrowerMock = mock(Borrower.class);
        Loan loanMock = mock(Loan.class);

        ArrayList<Loan> expect = new ArrayList<>();
        expect.add(loanMock);

        when(loanMock.getBook()).thenReturn(bookMock);
        when(borrowerMock.getBorrowedBooks()).thenReturn(expect);

        bookMock.makeHoldRequest(borrowerMock);
        verify(borrowerMock,times(2)).getBorrowedBooks();
    }

    // Prueba para verificar que se puede esperar un libro
    @Test
    public void test_makeHoldRequest2() throws  IllegalArgumentException{
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);

        Borrower borrowerMock = mock(Borrower.class);
        Loan loanMock = mock(Loan.class);

        ArrayList<Loan> expect = new ArrayList<>();
        expect.add(loanMock);

        when(loanMock.getBook()).thenReturn(nuevo);
        when(borrowerMock.getBorrowedBooks()).thenReturn(expect);

        bookMock.makeHoldRequest(borrowerMock);
        verify(borrowerMock,times(3)).getBorrowedBooks();
    }

    // Prueba para verificar que no permita hacer la misma solicitud
    @Test
    public void test_makeHoldRequest3() throws  IllegalArgumentException{
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);

        Borrower borrowerMock = mock(Borrower.class);
        Loan loanMock = mock(Loan.class);

        HoldRequest holdRequestMock = mock(HoldRequest.class);

        ArrayList<Loan> expect = new ArrayList<>();
        expect.add(loanMock);

        when(loanMock.getBook()).thenReturn(nuevo);
        when(borrowerMock.getBorrowedBooks()).thenReturn(expect);
        when(holdRequestMock.getBorrower()).thenReturn(borrowerMock);

        bookMock.setHoldRequests(holdRequestMock);

        bookMock.makeHoldRequest(borrowerMock);
        verify(holdRequestMock,times(1)).getBorrower();
    }

    // Prueba para verificar que se puede devolver correctamente los libros
    @Test
    public void test_returnBook() throws IllegalArgumentException{
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);

        Borrower borrowerMock = mock(Borrower.class);
        Loan loanMock = mock(Loan.class);
        Staff staffMock = mock(Staff.class);

        when(loanMock.getBook()).thenReturn(nuevo);
        when(borrowerMock.getName()).thenReturn("Diego");
        when(staffMock.getName()).thenReturn("Sofia");

        bookMock.returnBook(borrowerMock,loanMock,staffMock);

        Assert.assertEquals(borrowerMock.getBorrowedBooks().size(),0);
    }

    // Test para probar quitar una solicitud en espera tanto del libro como de quien lo presto
    @Test
    public void test_serviceHoldRequest() throws IllegalArgumentException{
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);

        Borrower borrowerMock = mock(Borrower.class);
        HoldRequest holdRequestMock = mock(HoldRequest.class);

        when(holdRequestMock.getBorrower()).thenReturn(borrowerMock);
        bookMock.setHoldRequests(holdRequestMock);
        borrowerMock.addHoldRequest(holdRequestMock);

        bookMock.serviceHoldRequest(holdRequestMock);

        Assert.assertEquals(bookMock.getHoldRequests(),borrowerMock.getOnHoldBooks());
    }

    // Prueba para solicitar un prestamo de un libro
    @Test
    public void test_issueBook() throws IllegalArgumentException{
        bookMock = new Book(this.id, this.title, this.subject, this.author, this.isIssued);

        Borrower borrowerMock = mock(Borrower.class);
        Staff staffMock = mock(Staff.class);

        when(staffMock.getName()).thenReturn("Sofia");
        when(borrowerMock.getName()).thenReturn("Diego");
        bookMock.issueBook(borrowerMock,staffMock);
    }


}
