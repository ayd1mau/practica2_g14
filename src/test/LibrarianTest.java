import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

public class LibrarianTest {
    protected int officeNo;     //Office Number of the Librarian
    protected static int currentOfficeNumber = 0;


    protected int id;
    protected String name;
    protected String address;
    protected int phoneNo;
    protected double salary;


    public Librarian librarianMock;


    private Librarian controller;
    private Staff greetingService;


    @Before
    public void setup() {
        officeNo = 1; //numero de oficina
        currentOfficeNumber = 0; //numero auxiliar
        name= "Marcos";
        address="zona.12";
        phoneNo=12345;
        salary=5000;
        id=1;

        greetingService = Mockito.mock(Staff.class);
        controller= Mockito.mock(Librarian.class);
        controller = new Librarian(id,name,address,phoneNo,salary,officeNo,greetingService);

    }

    @Test(expected = IllegalArgumentException.class)
    public void test_valid_phone_number() throws IllegalArgumentException {
        // Arrange
        phoneNo = -1;
        // Act
        librarianMock = new Librarian(this.id,this.name,this.address,this.phoneNo,this.salary,this.officeNo);

        // Assert
        Assert.fail();
        Assert.assertNotNull("El objeto es creado con exito",librarianMock);
        // Will throw
    }


    @Test
    public void test_currentNumbreOffice_is_same_as_set_in_constructor() throws IllegalArgumentException {
        // Arrange
        int expectedCurrentNumber = 1;

        // Act
        librarianMock = new Librarian(this.id,this.name,this.address,this.phoneNo,this.salary,this.officeNo);

        // Assert
        Assert.assertEquals(expectedCurrentNumber, librarianMock.officeNo);
    }




    @Test
    public void test_set_info_isnotnull() {
        // Arrange
        int expectedPhoneNumber = 44675262;
        // Act
        librarianMock = new Librarian(this.id,this.name,this.address,this.phoneNo,this.salary,this.officeNo);


        // Assert
       librarianMock.printInfo();
        Assert.assertNotNull(librarianMock.salary);
    }

    @Test
    public void test_changenregative_phoneNo_currentnumber() {
        // Arrange
        int expectedPhoneNumber = 44675262;
        // Act
        officeNo=15;
        librarianMock = new Librarian(this.id,this.name,this.address,this.phoneNo,this.salary,this.officeNo);


        // Assert

        Assert.assertSame(15,this.officeNo);
    }


    @Test
    public void test_sum_currentnumber() {
        // Arrange
        int expectedPhoneNumber = 44675262;
        // Act
        officeNo=15;
        librarianMock = new Librarian(this.id,this.name,this.address,this.phoneNo,this.salary,this.officeNo);
        librarianMock = new Librarian(this.id,this.name,this.address,this.phoneNo,this.salary,this.officeNo);

        // Assert
        Assert.assertEquals(7.0,Librarian.currentIdNumber,0);
    }


    @Test
    public void itShouldReturnTheServiceValueWith200StatusCode() {
        Mockito.when(greetingService.imprimir_info()).thenReturn("El salario es: "+ greetingService.salary);

        String valor= controller.imprimir_info();
        ArrayList<String> datos = new ArrayList<>();

        String datoss= valor.substring(3,10);
        System.out.println(datoss);
        datos.add(datoss);

            assertThat(
                    datos,
                    hasItems("salario"));

            Assert.assertTrue(valor.equals("El salario es: "+ controller.salary));

    }




}
