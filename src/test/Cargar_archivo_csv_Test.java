import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runners.model.TestClass;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Cargar_archivo_csv_Test {

    private Cargar_archivo_csv mockCargar;
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();
    @Before
    public void setUp() {
        mockCargar = Mockito.mock(Cargar_archivo_csv.class);
    }

    @Test
    public void testArchivo() throws IOException {
        Cargar_archivo_csv dto = new Cargar_archivo_csv();

        LinkedList<Persona> lista = new LinkedList<>();
        lista= dto.cargar_archivo();
        File fileTest = temporaryFolder.newFolder("datos.txt");
        //la idea es que cuando se alcance la linea de new File("/tmp/ReporteTest.txt")
        //me regrese el archivo creado con temporaryFolder
        Mockito.when(mockCargar.cargar_archivo()).thenReturn(lista);
        Assert.assertNotNull(lista);

    }

    @Test
    public void cantidadDatos() throws IOException {

        File fileTest = temporaryFolder.newFolder("datos.txt");

        Assert.assertTrue(fileTest.canRead());
        Assert.assertTrue(fileTest.getName().equals("datos.txt"));


    }
}
