import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

public class LoanTest {
    protected Borrower borrower;
    protected Book book;
    protected Staff issuer;
    protected Staff receiver;
    protected Date issuedDate;
    protected Date dateReturned;
    protected boolean finePaid;
    protected Loan loan;
    @Before
    public void setup(){
        this.borrower = new Borrower(1,"test","test","test",12345678);
        this.book = new Book(1,"test","test","test",true);
        this.issuer = new Staff(1,"test","test","test",12345677,5900.00);
        this.receiver = new Staff(2,"test2","test2","test2",12345678,7900.00);
        this.issuedDate=new Date("29/07/2020");
        this.dateReturned= new Date("29/09/2020");
        this.finePaid=true;
    }

    @Test
    public void test_set_get_book() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,this.finePaid);
        Book expectedBook= new Book(2,"test_setBook","test_setBook","test_setBook",true);;
        loan.setBook(expectedBook);
        Assert.assertSame(expectedBook, loan.getBook());
    }

    @Test
    public void test_set_get_ReturnedDate() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,this.finePaid);
        Date expectedReturnedDate= new Date("1/01/2020");
        loan.setReturnedDate(expectedReturnedDate);
        Assert.assertEquals(expectedReturnedDate, loan.getReturnDate());
    }
    @Test
    public void test_set_get_FineStatus() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,this.finePaid);
        boolean expectedFineStatus=true;
        loan.setFineStatus(expectedFineStatus);
        Assert.assertTrue( loan.getFineStatus());
        expectedFineStatus=false;
        loan.setFineStatus(expectedFineStatus);
        Assert.assertFalse( loan.getFineStatus());
    }
    @Test
    public void test_set_get_Receiver() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,this.finePaid);
        Staff expectedReceiver=new Staff(2,"test2_Receiver","test2_Receiver","test2_Receiver",987654321,7777.00);
        loan.setReceiver(expectedReceiver);
        Assert.assertEquals(expectedReceiver, loan.getReceiver());
    }

    @Test
    public void test_computeFine1() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,true);
        int expectetotalFine=0;
        Assert.assertEquals(expectetotalFine, loan.computeFine1(),0);
    }

    //otros tests
    @Test
    public void test_set_get_Issuer() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,this.finePaid);
        Staff expectedIssuer= new Staff(1,"pass","test","tests",11222233,8900);
        loan.setReceiver(expectedIssuer);
        Assert.assertNotSame(expectedIssuer, loan.getIssuer());
        Assert.assertSame(this.issuer, loan.getIssuer());
    }

    @Test
    public void test_set_get_IssuedDate() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,this.finePaid);
        Assert.assertEquals(this.issuedDate, loan.getIssuedDate());

    }

    @Test
    public void test_PayFine() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,true);
        boolean expectPayFine=false;
        loan.payFine();
        Assert.assertNotSame(expectPayFine,loan.getFineStatus());
    }

    @Test
    public void test_renewIssuedBook() {
        loan = new Loan(this.borrower,this.book,this.issuer,this.receiver,this.issuedDate,this.dateReturned,true);
        Date expectedDate= new Date("1/01/2020");
        loan.renewIssuedBook(expectedDate);
        Assert.assertEquals(expectedDate,loan.getIssuedDate());
    }










}
